$(document).ready(function() {

// ------------------------------counter-------------------------------------------------
    function counter(type, disp) {
    	$('#tarea').focus();
    	var disptime = disp.split(':');
        var minute = parseInt(disptime[0], 10);
        var second = parseInt(disptime[1], 10);
        var displaytimer = setInterval(function() {
            second--;
            minute = (second < 0) ? minute - 1 : minute;
            //if(second < 0)
    		// {
    		// 	minute--;
    		// 	var curspeed = $('#tarea').val().split(' ').length;
    		// 	$('.speed span').text(curspeed/minute);
    		// 	$('.words span').text(curspeed);
    		// }
            second = (second < 0) ? 59 : second;
            second = (second < 10) ? '0' + second : second;
            disp = minute + ':' + second;
            $('.timer').text(disp);

// ------------------------------in the end----------------------------------------          
            if (minute < 0) {
                clearInterval(displaytimer);
                $('.timer-btn').attr("disabled", false);
                speed(type);
                $('.timer').text("Timer");
                $('.stop').addClass("hide");
    			$('.start').removeClass("hide");
                // $('#tarea').val('');
            }

// ---------------------------------in the middle------------------------------------
            $('.stop').click(function(){
    			clearInterval(displaytimer);
    			var words = $('#tarea').val().split(' ').length;
    			words = ($('#tarea').val() == '') ? 0 : words;
		        var speed = (minute < 1) ? "Type for atleast 1min" : words/minute;
		        $('.speed span').text(speed);
		        $('.words span').text(words);
    			$('.timer-btn').attr("disabled", false);
    			$('.timer').text("Timer");
    			$('.stop').addClass("hide");
    			$('.start').removeClass("hide");
    		});
        }, 1000);
    }


// ---------------------------------------------Speed-------------------------------------------------
    function speed(type) {
        var words = $('#tarea').val().split(' ').length;
        words = ($('#tarea').val() == '') ? 0 : words;
        var speed = words / type;
        // alert("Your Typing speed is " + speed);
        $('.overlay').removeClass("hide");
        $('.speed span').text(speed);
        $('.words span').text(words);
    }

    $('.popclose').click(function(){
    	$('.overlay').addClass("hide");
    })

// --------------------------------------------free typing---------------------------------------------
    function freetype(){
    	$('#tarea').focus();
    	var second = 0;
    	var minute = 0;
    	var freetimer = setInterval(function(){
    		second++;
    		// minute = (second > 59) ? minute+1 : minute;
    		if(second > 59)
    		{
    			minute++;
    			var curspeed = $('#tarea').val().split(' ').length;
    			$('.speed span').text(curspeed/minute);
    			$('.words span').text(curspeed);
    		}
    		second = (second > 59) ? 0 : second;
    		second = (second < 10) ? '0'+second : second;
    		var disp = minute + ':' + second;
            $('.timer').text(disp);
            $('.stop').click(function(){
    			clearInterval(freetimer);
    			// speed(minute);
    			var words = $('#tarea').val().split(' ').length;
		        var speed = (minute < 1) ? "Type for atleast 1min" : words/minute;
		        $('.speed span').text(speed);
		        words = ($('#tarea').val() == '') ? 0 : words;
		        $('.words span').text(words);
    			$('.timer-btn').attr("disabled", false);
    			$('.timer').text("Timer");
    			$('.stop').addClass("hide");
    			$('.start').removeClass("hide");
    			// location.reload();
    		});
    	}, 1000);
    }

// ---------------------------------------------------------------------------------------------------------

    function btninit(){
    	$('.timer-btn').attr("disabled", true);
        $('#tarea').val('');
        $('.start').addClass("hide");
    	$('.stop').removeClass("hide");
    }

// ---------------------------------------------Timer Buttons----------------------------------------------

	$('.1min').click(function() {
    	btninit();
        counter(1, "1:00");
    })

    $('.3min').click(function() {
        btninit();
        counter(3, "3:00");
    })

    $('.5min').click(function() {
        btninit();
        counter(5, "5:00");
    })

    $('.manual').click(function(){
    	var time = prompt("How much minutes you want to set?");
    	if(time != null){
    		$('.timer-btn').attr("disabled", true);
        	$('#tarea').val('');
    		var minute = time+":00";
    		counter(time, minute);
    	}
    })

    $('.dropdown-trigger').dropdown();

    $('.start').click(function(){
    	btninit();
    	freetype();
    })

// ----------------------------------------------Ctrl Buttons------------------------------------------------

    $('.reloadpg').click(function(){
    	location.reload();
    })

    $('.clip').click(function(){
    	$('#tarea').select();
    	document.execCommand("copy");

    })

    $('.clrscr').click(function(){
    	$('#tarea').val('');
    })
});


// -----------------------------------------------Tooltip-------------------------------------------

$(document).ready(function(){
    $('.tooltipped').tooltip();
  });